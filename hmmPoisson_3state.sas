
/*0.1 0.8 0.1*/
%let pi1 = 0.1;
%let pi2 = 0.9;
/*0.7 0.2 0.1*/
%let a11 = 0.7;
%let a12 = 0.9;
/*0.1 0.8 0.1*/
%let a21 = 0.1;
%let a22 = 0.9;
/*0.1 0.3 0.6*/
%let a31 = 0.1;
%let a32 = 0.4;
%let lambda1 = 0.1;
%let lambda2 = 4;
%let lambda3 = 8;
 
%macro dgpPsHMM(table,T,nSections,seed);

data &table;
call streaminit(&seed.); 

secLen = ceil(&T./&nSections.);
do t = 1 to &T.;
sec = floor((t-1)/secLen)+1;
if(t=1) then do;

/* initial probability distribution */
p1 = &pi1.;
p2 = &pi2.;
end;
else do;
/* transition probability matrix */
if(lags=1) then 
  do; 
    p1 = &a11.;
    p2 = &a12.;
  end;
else if(lags=2) then
  do;
    p1 = &a21.;
    p2 = &a22.;
  end;
else 
  do;
   p1 = &a31.;
   p2 = &a32.;
  end;
  end;
u = rand("Uniform");
if(u<=p1) then s=1;
else if(p1<u<=p2) then s = 2;
else s=3;

if(s=1) then do;
/* at state 1 */
y = rand("Poisson",&lambda1.);
end;
else if(s=2) then do;
/* at state 2 */
y = rand("Poisson",&lambda2.);
end;
else do;
/* at state 3 */
y = rand("Poisson",&lambda3.);
end;
output;
lags = s; 
end;
run;
proc sort data=&table.; by sec t; run;%mend;

%let T = 1000 ;
%let nSections = 2;
%let seed = 1234;
%dgpPsHMM(hmmpsdt,&T.,&nSections.,&seed.);



/**/
/*Specify your CAS setup*/
/*%let sysparm =host:rdcgrd310+box:no; */
/*%cassetup;*/


/*Optional: run model_selection.sas*/

data sascas1.hmmpsdt;
   set hmmpsdt;
run;


proc sgplot data=hmmpsdt;
   series x=t y=y ;
   where t<200;
   xaxis integer;
   yaxis integer;
run;


proc means data=hmmpsdt mean var max;
   var y;
run;
/* draw histogram of frequency of zeros */
proc sgplot data=hmmpsdt;
   histogram y/  datalabel = percent;
   xaxis integer;
run;


ods output Lambda= my_lambda;
proc hmm data=sascas1.hmmpsdt;
	id time=t  /*section=sec*/;
	model y  / nstate=3 type=Poisson;
	filter    out=sasout1.myeflt;
	smooth    out=sasout1.myesmth;
	forecast  out=sasout1.myfo outall=sasout1.myfoall lead=1; 
	evaluate  out=sasout1.myeval;
	decode    out=sasout1.mydecode;
	score     outmodel=sasout1.scmdl;
	optimize  algorithm=ipdirect printlevel=3;
run; 

proc print data=sasout1.myfo; run;

proc print data=sasout1.myfoall; run;


   data mydecode;
     set sasout1.mydecode;
   run;

data ex.merged;
merge my_lambda(in=in1)   sasout1.mydecode(in=in2);
by state;
if in1 and in2;
keep  state estimation t;
run;

proc sort data=ex.merged;
  by t;
run;

data ex.merged2;
merge ex.merged(in=in1)   sascas1.hmmpsdt(in=in2);
by t;
if in1 and in2;
keep  state estimation t y;
run;


/* Plot the subset of data */
proc sgplot data=ex.merged2;
  series x=t y=y / lineattrs=(color=red);
  series x=t y=estimation / lineattrs=(color=blue);
/*  xaxis label='Year' labelattrs=(color=red);;*/
/*  yaxis label='Count / Estimation' labelattrs=(color=red);*/
  where t<200;
run;


   

   cas sascas1 terminate; 
	%if %symexist(_CASPORT_) 
    %then %do; 
       proc casoperate; 
   		 shutdown; 
       run; */y;

       quit; 
    %end;

