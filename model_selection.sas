


data sascas1.hmmpsdt(promote=yes);
   set hmmpsdt;
run;


proc cas;
  hiddenMarkovModel.hmm /
  table = 'hmmpsdt',
  id={time='t'},
  model={depvars={'y'}, type='POISSON', nState=2},
  optimize = {algorithm='ipdirect', printlevel=1},
  display={excludeAll=TRUE};
run;
quit;


 /*Step 1: Pack it into an action for simplicity */
proc cas;
   table.dropTable / table='myhmm', quiet=True; 
   builtins.defineActionSet / name = "myhmm"
      actions={
         {
            name = "runone"
            desc = "Wrapper to a call to hiddenMarkovModel.hmm "
            parms = {
/*			   {name="seed"  type="int"    required=TRUE}*/
              {name="state" type="int"    required=FALSE, default=2}
              {name="tag"   type="int"    required=TRUE,  default=1111}
            }
            definition = "
                loadActionset 'hiddenMarkovModel';
                outtablename='BIC'||(String)tag;
                hiddenMarkovModel.hmm result=r / 
                table = 'hmmpsdt',  
                id={time='t'},
                model={depvars={'y'},  type='POISSON', nState=state},
                optimize = {algorithm='ipdirect', printlevel=1},
                outstat={name=outtablename, replace=True};
                fetch result=r / table=outtablename;
                print(r);
                aic = 0+r[1][1]['AIC'];  /* add zero to convert to number */
                aicc= 0+r[1][1]['AICC'];
                bic = 0+r[1][1]['BIC'];
                hqc = 0+r[1][1]['HQC'];
                logl= 0+r[1][1]['LogLikelihood'];
 
                print('AIC ='  || (String)aic); 
                print('AICC='  || (String)aicc); 
                print('BIC ='  || (String)bic); 
                print('HQC ='  || (String)hqc); 
                print('LOGL='  || (String)logl);
                print('tag='   || (String)tag);
                print('state=' || (String)state);

                f['aic']  = aic;
                f['aicc'] = aicc;
                f['bic']  = bic;
                f['hqc']  = hqc;
                f['logl'] = logl;
                send_response(f);
            "
         }
      };
   run;
quit;  

/* Step 2: Test the user-defined CAS action;  */
proc cas;
   myhmm.runone result=r /
/*      seed  = 10019, */
      state = 3
      tag   = 4;
   print r;   
run;
quit;   


/* Step 3: save to a table and promote */
proc cas;
   actionSetToTable /
      actionSet='myhmm',
      casOut={name="myhmm", promote=True }; 
run;
quit; 

/* Step 4: call solveblackbox to run queue of parallel jobs */
proc cas noqueue;
   nsessions=10;  /* Number of sessions to create, can't be higher than 40 */   
   nworkers=1;   /* number of workers per session */
   /* Note total number of nodes used = (nsessions*nworkers)! */

   njobs=10;     /* number of hmm seeds/states you wish to sample
                    this number can now be as large as you want*/
  
   /* The CASL code for evaluating the objective */
   source caslEval;
      filename='~/output/hmm_log_iter_' || (String)_bbEvalTag_ || '.txt';
      file nodes filename;
      actionSetFromTable / name="myhmm", table={name="myhmm"};
      myhmm.runone result=f_obj / state=varstate, tag=_bbEvalTag_; 
      send_response(f_obj);   
   endsource;

   /* Invoke the solveBlackbox action */
   optimization.solveBlackbox /
      decVars = {
         /* You can relax the bounds if you want to look at state as well */
         {name='varstate', type='I', lb=1 , ub=10 } 
      },
      obj = {{name='bic', type='min'}}, 
      /* Add dummy constriants (if you want) to keep track of other optional values */
      con = {{name='aicc', lb=-1e20, ub=1e20},
             {name='aic',  lb=-1e20, ub=1e20},
             {name='hqc',  lb=-1e20, ub=1e20},
             {name='logl', lb=-1e20, ub=1e20}}
      func = {eval=caslEval, nSubsessionWorkers=nworkers},
      primalOut={name="p_out", replace=true}
      cacheOut={name="c_out", replace=true}
      nParallel=nsessions,
      popsize=njobs,
      maxGen=1,
      nlocal=0
   ;
run; 
quit;


proc sort data=sascas1.c_out(rename=(varstate=State aic=AIC)) out=ex.aic;
  by State;
run;

data ex.aic;
  set ex.aic;
  keep state aic;
run;

proc print data=ex.aic nobos label;
label state='Number of State'
      aic = 'AIC';
run;

